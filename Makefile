KEEP=.svg|.tex|Makefile
INIT=*.odt
CLS=*~ *.log *.aux *.out *.bak *.toc *.pl
TEXFILES = $(wildcard *.tex)
SVGFILES = $(wildcard *.svg)

all: pdf

%.pdf: %.tex
	pdflatex  $<
	pdflatex  $<

%_img.pdf: %.svg
	inkscape --export-type="pdf" --export-filename="$@" $<

clean:
	find . -maxdepth 1 -type f |egrep -v '($(KEEP))$$' | xargs rm -f	

pdf: img $(patsubst %.tex,%.pdf,$(TEXFILES))

img: $(patsubst %.svg,%_img.pdf,$(SVGFILES)) 
